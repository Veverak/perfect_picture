// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

pub fn apply_draw_mask(
    split_id: crate::types::SplitId,
) {
    let split = picture.split(split_id);
    let polygon = geo::Polygon::new(std::mem::take(points).into(), vec![]);
    let polygon = geo::algorithm::simplify::Simplify::simplify(
        &polygon,
        &(0.5 / split.scale.min(1.)),
    );
    let mask = &mut picture.mask.value;
    mask.polygon = if shift {
        geo_booleanop::boolean::BooleanOp::difference(&mask.polygon, &polygon)
    } else {
        geo_booleanop::boolean::BooleanOp::union(&mask.polygon, &polygon)
    };
    picture.mask_intersection = None;
    state.notify_views(env);
    state.gesture = None;
}
