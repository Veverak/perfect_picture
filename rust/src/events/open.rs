// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::types;

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeOpen(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    image: crate::bindings::Bitmap,
) {
    let size_x = image.get_width(env);
    let size_y = image.get_height(env);
    let mut image_data = vec![0; 4 * size_x as usize * size_y as usize];
    let buffer = env.new_direct_byte_buffer(&mut image_data).unwrap();
    image.copy_pixels_to_buffer(env, buffer.into());
    let mut histogram = [[0; 3]; 256];
    let mut source_index = 0;
    for _ in 0..size_x * size_y {
        histogram[image_data[source_index] as usize][0] += 1;
        histogram[image_data[source_index + 1] as usize][1] += 1;
        histogram[image_data[source_index + 2] as usize][2] += 1;
        source_index += 4;
    }
    let mut state = crate::state();
    let state = state.get();
    state.picture = Some(types::Picture {
        band_stop_course: 8,
        band_stop_feather1: 4,
        band_stop_feather2: 4,
        band_stop_fine: 4,
        band_stop_opacity: 255,
        blur_feather1: 4,
        blur_feather2: 4,
        blur_radius: 4,
        clone_inpaint_context_size: 64.,
        clone_inpaint_outline: 6.,
        clone_offset: types::Point { x: 0., y: 0. },
        clone_offset_selected: false,
        crop: types::Setting::new(types::BoundingBox {
            position_x_min: 0,
            position_x_max: size_x,
            position_y_min: 0,
            position_y_max: size_y,
        }),
        crop_aspect_ratio: (0, 0),
        crop_mask_opacity: 229,
        export_format: types::Format::Jpeg,
        export_quality: 90,
        export_scale_down: false,
        histogram,
        histogram_image: None,
        image: image.global(env),
        inpaint_context_size: 64.,
        intersect_masks: false,
        last_filter: None,
        levels: types::Setting::new(Default::default()),
        levels_filter_feather1: 10,
        levels_filter_feather2: 10,
        levels_filter_levels: Default::default(),
        light: types::Light {
            brightness: 10,
            saturation: 0,
        },
        light_feather1: 10,
        light_feather2: 10,
        mask: types::Setting::new(types::Mask {
            polygon: geo::MultiPolygon(vec![]),
        }),
        mask_color: 0x7fff0000,
        mask_intersection: None,
        size: types::Size {
            x: size_x,
            y: size_y,
        },
        split_position_y: 0,
        split_screen: false,
        split1: types::Split {
            pan_x: 0.,
            pan_y: 0.,
            scale: 0.,
        },
        split2: types::Split {
            pan_x: 0.,
            pan_y: 0.,
            scale: 0.,
        },
        toolbar_visible: None,
        undo: None,
        unsharp_mask_feather1: 4,
        unsharp_mask_feather2: 4,
        unsharp_mask_radius: 4,
        unsharp_mask_threshold: 4,
        working: false,
    });
    state.notify_views(env);
}
