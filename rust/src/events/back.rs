// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_onBackPressed(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
) {
    let mut state = crate::state();
    let state = state.get();
    if let Some(picture) = &mut state.picture {
        picture.clone_offset_selected = false;
        picture.toolbar_visible = None;
        state.notify_views(env);
    }
}
