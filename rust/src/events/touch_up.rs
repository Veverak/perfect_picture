// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::gesture::Gesture;

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeTouchUp(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    shift: bool,
    position_x: f32,
    position_y: f32,
    view: crate::bindings::View,
) {
    let mut state = crate::state();
    let state = state.get();
    if let Some(picture) = &mut state.picture {
        match state.gesture.take() {
            Some(Gesture::CropEdge(gesture)) => {
                picture.crop(gesture, position_x, position_y);
                view.invalidate(env);
            }
            Some(Gesture::DrawMask(gesture)) => {
                gesture.apply(env, picture, shift, view);
            }
            _ => {}
        }
    }
}
