// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    bindings,
    gesture::{self, Channel, Extreme, Gesture},
    toolbars::Button,
    types::{Point, SplitId, Toolbar},
};
use std::cmp::Ordering::{Equal, Greater, Less};

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeTouchDown(
    env: jni::JNIEnv,
    _caller: bindings::Context,
    position_x: f32,
    position_y: f32,
    view: bindings::View,
) {
    let mut state = crate::state();
    let state = state.get();
    match crate::toolbars::button_at_point(env, state, position_x, position_y, view) {
        Button::Fallthrough => {
            if let Some(picture) = &mut state.picture {
                let density = view.get_display_metrics(env).scaled_density(env);
                let split_position_y = picture.split_position_y as f32 + 0.5;
                let split_id =
                    if !picture.split_screen || position_y < split_position_y - 8. * density {
                        SplitId::Split1
                    } else {
                        if position_y < split_position_y + 8. * density {
                            state.gesture = Some(Gesture::Split);
                            return;
                        }
                        SplitId::Split2
                    };
                let split = picture.split(split_id);
                if picture.toolbar_visible == Some(Toolbar::Clone) && picture.clone_offset_selected
                {
                    state.gesture = Some(Gesture::CloneOffset {
                        position: Point {
                            x: picture.clone_offset.x - position_x / split.scale,
                            y: picture.clone_offset.y - position_y / split.scale,
                        },
                        split_id,
                    });
                } else if picture.toolbar_visible == Some(Toolbar::Crop) {
                    let position_x = (position_x - split.pan_x) / split.scale;
                    let position_y = (position_y - split.pan_y) / split.scale;
                    let crop = picture.crop.value;
                    let center_x = (crop.position_x_min + crop.position_x_max) as f32;
                    let center_y = (crop.position_y_min + crop.position_y_max) as f32;
                    let crop_size = crop.size();
                    let distance_x = (2. * position_x - center_x) / crop_size.x as f32;
                    let distance_y = (2. * position_y - center_y) / crop_size.y as f32;
                    let attachment_x = if distance_x < -2. / 3. {
                        Less
                    } else if distance_x > 2. / 3. {
                        Greater
                    } else {
                        Equal
                    };
                    let attachment_y = if distance_y < -2. / 3. {
                        Less
                    } else if distance_y > 2. / 3. {
                        Greater
                    } else {
                        Equal
                    };
                    state.gesture = Some(if attachment_x == Equal && attachment_y == Equal {
                        Gesture::CropCenter {
                            position: Point {
                                x: crop.position_x_min as f32 - position_x,
                                y: crop.position_y_min as f32 - position_y,
                            },
                            split_id,
                        }
                    } else {
                        Gesture::CropEdge(gesture::CropEdge {
                            attachment_x,
                            attachment_y,
                            split_id,
                        })
                    });
                } else {
                    state.gesture = Some(Gesture::DrawMask(crate::gesture::DrawMask::new(
                        Point {
                            x: position_x,
                            y: position_y,
                        },
                        split,
                        split_id,
                    )));
                }
            }
        }
        Button::Levels(channel) => {
            levels(env, channel, false, position_x, state, view);
        }
        Button::LevelsFilter(channel) => {
            levels(env, channel, true, position_x, state, view);
        }
        Button::None => {}
        Button::Some(gesture) => {
            state.gesture = Some(Gesture::Button(gesture));
        }
    }
}

fn levels(
    env: jni::JNIEnv,
    channel: Channel,
    filter: bool,
    position_x: f32,
    state: &mut crate::initialize::State,
    view: bindings::View,
) {
    let picture = state.picture.as_mut().unwrap();
    let density = view.get_display_metrics(env).scaled_density(env);
    let levels = if filter {
        &mut picture.levels_filter_levels
    } else {
        &mut picture.levels.value
    };
    let channel_levels = match channel {
        Channel::Master => levels.master(),
        Channel::Red => levels.red,
        Channel::Green => levels.green,
        Channel::Blue => levels.blue,
    };
    let margin = (8. * density).round();
    let view_size_x = view.get_width(env);
    let level = 255. * (position_x - margin) / (view_size_x as f32 - 2. * margin);
    let extreme = if 2. * (level as f32) < channel_levels.0 as f32 + channel_levels.1 as f32 {
        Extreme::Min
    } else {
        Extreme::Max
    };
    let gesture = gesture::Levels { channel, extreme };
    levels.apply(gesture, level);
    state.gesture = Some(if filter {
        Gesture::LevelsFilter(gesture)
    } else {
        Gesture::Levels(gesture)
    });
    state.notify_views(env);
}
