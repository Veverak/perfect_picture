// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{bindings, gesture::Gesture};

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeKeyDown(
    env: jni::JNIEnv,
    caller: bindings::Context,
    key_code: i32,
    scan_code: i32,
    view: bindings::View,
) -> bool {
    let mut state = crate::state();
    let state = state.get();
    match state.gesture.take() {
        Some(Gesture::Button(gesture)) => {
            state.gesture = None;
            state.key_bindings.insert(scan_code, gesture.action);
            return true;
        }
        Some(Gesture::KeyboardDrawMask(gesture)) => {
            if key_code == 67 {
                if let Some(picture) = &mut state.picture {
                    gesture.apply(env, picture, true, view);
                }
            }
        }
        Some(Gesture::Pointer(point)) => match key_code {
            57 | 58 => {
                if let Some(picture) = &mut state.picture {
                    if let Some(split_id) = picture.split_at_position(point.y) {
                        let split = picture.split(split_id);
                        state.gesture = Some(Gesture::KeyboardDrawMask(
                            crate::gesture::DrawMask::new(point, split, split_id),
                        ));
                    }
                }
                return true;
            }
            _ => {
                if let crate::toolbars::Button::Some(gesture) =
                    crate::toolbars::button_at_point(env, state, point.x, point.y, view)
                {
                    state.key_bindings.insert(scan_code, gesture.action);
                    return true;
                }
            }
        },
        _ => {}
    }
    if let Some(&action) = state.key_bindings.get(&scan_code) {
        crate::action::perform_action(action, caller, env, state);
        true
    } else {
        false
    }
}
