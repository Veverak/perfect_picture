// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{gesture::Gesture, types};

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeScroll(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    position_x: f32,
    position_y: f32,
    view: crate::bindings::View,
) {
    let mut state = crate::state();
    let state = state.get();
    let picture = match &mut state.picture {
        None => return,
        Some(value) => value,
    };
    match &mut state.gesture {
        Some(Gesture::Button(gesture)) => {
            if position_x < gesture.bounding_box.position_x_min
                || position_x >= gesture.bounding_box.position_x_max
                || position_y < gesture.bounding_box.position_y_min
                || position_y >= gesture.bounding_box.position_y_max
            {
                state.gesture = None;
            }
        }
        Some(Gesture::CloneOffset { position, split_id }) => {
            let scale = picture.split(*split_id).scale;
            picture.clone_offset = types::Point {
                x: position.x + position_x / scale,
                y: position.y + position_y / scale,
            };
            view.invalidate(env);
        }
        Some(Gesture::CropCenter { position, split_id }) => {
            let split = picture.split(*split_id);
            let position_x = (position_x - split.pan_x) / split.scale;
            let position_y = (position_y - split.pan_y) / split.scale;
            let crop = &mut picture.crop.value;
            let crop_size = crop.size();
            let position_x_min = ((position.x + position_x).round() as i32)
                .min(picture.size.x - crop_size.x)
                .max(0);
            let position_y_min = ((position.y + position_y).round() as i32)
                .min(picture.size.y - crop_size.y)
                .max(0);
            if position_x_min != crop.position_x_min || position_y_min != crop.position_y_min {
                *crop = types::BoundingBox {
                    position_x_min,
                    position_x_max: position_x_min + crop_size.x,
                    position_y_min,
                    position_y_max: position_y_min + crop_size.y,
                };
                view.invalidate(env);
            }
        }
        Some(Gesture::CropEdge(gesture)) => {
            picture.crop(*gesture, position_x, position_y);
            view.invalidate(env);
        }
        Some(Gesture::DrawMask(gesture)) => {
            gesture.add_point(env, picture, position_x, position_y, view);
        }
        Some(Gesture::Levels(gesture)) => {
            let density = view.get_display_metrics(env).scaled_density(env);
            let margin = (8. * density).round();
            let view_size_x = view.get_width(env);
            let level = 255. * (position_x - margin) / (view_size_x as f32 - 2. * margin);
            picture.levels.value.apply(*gesture, level);
            view.invalidate(env);
        }
        Some(Gesture::LevelsFilter(gesture)) => {
            let density = view.get_display_metrics(env).scaled_density(env);
            let margin = (8. * density).round();
            let view_size_x = view.get_width(env);
            let level = 255. * (position_x - margin) / (view_size_x as f32 - 2. * margin);
            picture.levels_filter_levels.apply(*gesture, level);
            view.invalidate(env);
        }
        Some(Gesture::Split) => {
            let position_y = position_y.round() as i32;
            if position_y != picture.split_position_y {
                picture.split_position_y = position_y;
                view.invalidate(env);
            }
        }
        _ => {}
    }
}
