// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    bindings,
    gesture::Gesture,
    toolbars::{TEXT_COLOR, TEXT_SIZE},
    types::{self, Point, SplitId, Toolbar},
};

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeDraw(
    env: jni::JNIEnv,
    _caller: bindings::Context,
    canvas: bindings::Canvas,
    view: bindings::View,
) {
    let density = view.get_display_metrics(env).scaled_density(env);
    let toolbar_size_y = crate::toolbars::toolbar_size_y(density);
    let view_size_x = view.get_width(env);
    let mut state = crate::state();
    let state = state.get();
    let view_size_y = view.get_height(env) - state.inset_bottom;
    let buttons_position_y_min =
        view_size_y - crate::toolbars::toolbar_count(&state.picture) * toolbar_size_y;
    let paint = bindings::Paint::new(env);
    let align_center = bindings::Align::center(env);
    let align_left = bindings::Align::left(env);
    let align_right = bindings::Align::right(env);
    paint.set_text_align(env, align_center);
    paint.set_text_size(env, TEXT_SIZE * density);
    let picture = match &mut state.picture {
        None => {
            paint.set_color(env, TEXT_COLOR);
            canvas.draw_text(
                env,
                bindings::string(env, "Open"),
                Point {
                    x: view_size_x as f32 / 2.,
                    y: buttons_position_y_min as f32 + 28. * density,
                },
                paint,
            );
            return;
        }
        Some(value) => value,
    };
    let gesture = &state.gesture;
    let inset_top = state.inset_top;
    let draw_split = |picture: &mut _, split_id| {
        draw_split(
            env,
            buttons_position_y_min,
            canvas,
            density,
            gesture,
            inset_top,
            paint,
            picture,
            split_id,
            view_size_x,
            view_size_y,
        )
    };
    if picture.split_screen {
        if picture.split_position_y <= state.inset_top
            || picture.split_position_y >= buttons_position_y_min
        {
            picture.split_position_y = (buttons_position_y_min - state.inset_top) / 2;
        }
        canvas.save(env);
        canvas.clip_rectangle(
            env,
            types::BoundingBox {
                position_x_min: 0,
                position_x_max: view_size_x,
                position_y_min: 0,
                position_y_max: picture.split_position_y,
            },
        );
        draw_split(picture, SplitId::Split1);
        canvas.restore(env);
        canvas.save(env);
        canvas.clip_rectangle(
            env,
            types::BoundingBox {
                position_x_min: 0,
                position_x_max: view_size_x,
                position_y_min: picture.split_position_y + 1,
                position_y_max: view_size_y,
            },
        );
        paint.set_color(env, 0xffffffff);
        draw_split(picture, SplitId::Split2);
        canvas.restore(env);
    } else {
        draw_split(picture, SplitId::Split1);
    }
    paint.set_color(env, 0x66000000);
    canvas.draw_rectangle(
        env,
        0.,
        buttons_position_y_min as f32,
        view_size_x as f32,
        view_size_y as f32,
        paint,
    );
    let mut display = crate::toolbars::display::Display {
        inner: crate::toolbars::display::DisplayInner {
            align_center,
            align_left,
            align_right,
            canvas,
            density,
            env,
            paint,
            position_y_max: buttons_position_y_min.into(),
            toolbar_size_y,
            view_size_x,
        },
        picture,
    };
    paint.set_color(env, TEXT_COLOR);
    crate::toolbars::draw(&mut display);
}

fn draw_split(
    env: jni::JNIEnv,
    buttons_position_y_min: i32,
    canvas: bindings::Canvas,
    density: f32,
    gesture: &Option<Gesture>,
    inset_top: i32,
    paint: bindings::Paint,
    picture: &mut types::Picture,
    split_id: SplitId,
    view_size_x: i32,
    view_size_y: i32,
) {
    let size = picture.size;
    let split = picture.split_mut(split_id);
    if split.scale == 0. {
        let viewport_size_x = view_size_x;
        let viewport_size_y = buttons_position_y_min - inset_top;
        split.scale = if size.x * viewport_size_y > size.y * viewport_size_x {
            viewport_size_x as f32 / size.x as f32
        } else {
            viewport_size_y as f32 / size.y as f32
        };
        split.pan_y =
            (viewport_size_y as f32 - split.scale * size.y as f32) / 2. + inset_top as f32;
        split.pan_x = (viewport_size_x as f32 - split.scale * size.x as f32) / 2.;
    }
    let split = picture.split(split_id);
    let source = bindings::Rect::new(env, 0, 0, size.x, size.y);
    let origin = Point {
        x: split.pan_x,
        y: split.pan_y,
    };
    let scale = split.scale;
    let transform_x = |position| origin.x + scale * position;
    let transform_y = |position| origin.y + scale * position;
    let transform = |point: Point| Point {
        x: transform_x(point.x),
        y: transform_y(point.y),
    };
    let destination = bindings::RectF::new(
        env,
        origin.x,
        origin.y,
        transform_x(size.x as f32),
        transform_y(size.y as f32),
    );
    let color_filter = picture.levels.value.color_filter(env);
    paint.set_color_filter(env, color_filter);
    paint.set_filter_bitmap(env, false);
    canvas.draw_bitmap(env, picture.image.as_obj(), source, destination, paint);
    paint.set_color_filter(env, bindings::null());
    let crop = picture.crop.value;
    paint.set_color(env, (picture.crop_mask_opacity as u32) << 24);
    if crop.position_x_min > 0 {
        canvas.draw_rectangle(
            env,
            transform_x(0.),
            transform_y(0.),
            transform_x(crop.position_x_min as _),
            transform_y(size.y as _),
            paint,
        );
    }
    if crop.position_x_max < size.x {
        canvas.draw_rectangle(
            env,
            transform_x(crop.position_x_max as _),
            transform_y(0.),
            transform_x(size.x as _),
            transform_y(size.y as _),
            paint,
        );
    }
    if crop.position_y_min > 0 {
        canvas.draw_rectangle(
            env,
            transform_x(crop.position_x_min as _),
            transform_y(0.),
            transform_x(crop.position_x_max as _),
            transform_y(crop.position_y_min as _),
            paint,
        );
    }
    if crop.position_y_max < size.y {
        canvas.draw_rectangle(
            env,
            transform_x(crop.position_x_min as _),
            transform_y(crop.position_y_max as _),
            transform_x(crop.position_x_max as _),
            transform_y(size.y as _),
            paint,
        );
    }
    let transform_bounding_box = |bounding_box: types::BoundingBox<i32>| types::BoundingBox {
        position_x_min: transform_x(bounding_box.position_x_min as f32).floor() as _,
        position_x_max: transform_x(bounding_box.position_x_max as f32).ceil() as _,
        position_y_min: transform_y(bounding_box.position_y_min as f32).floor() as _,
        position_y_max: transform_y(bounding_box.position_y_max as f32).ceil() as _,
    };
    let draw_mask_bounding_box =
        |mask: &types::Mask, picture: &types::Picture, context_size: f32| {
            paint.set_color(env, picture.mask_color);
            paint.set_stroke_width(env, density);
            paint.set_style(env, bindings::Style::stroke(env));
            let bounding_box = types::mask_bounding_box(context_size, mask, picture);
            canvas.draw_rectangle(
                env,
                transform_x(bounding_box.position_x_min as _),
                transform_y(bounding_box.position_y_min as _),
                transform_x(bounding_box.position_x_max as _),
                transform_y(bounding_box.position_y_max as _),
                paint,
            );
        };
    let draw_feathered_mask = |feather1, feather2, punch_hole| {
        if picture.mask.value.is_empty() {
            return;
        }
        if picture.intersect_masks {
            if picture.mask.alternative_value.is_empty() {
                return;
            }
            let bounding_box =
                match picture.mask_intersection_bounding_box(feather1 as _, feather2 as _) {
                    None => return,
                    Some(bounding_box) => transform_bounding_box(bounding_box),
                };
            let mask_image = bindings::Bitmap::create_bitmap_uninitialized(
                env,
                bounding_box.size(),
                bindings::BitmapConfig::argb_8888(env),
            );
            let mask_canvas = bindings::Canvas::new(env, mask_image);
            mask_canvas.translate(env, bounding_box.point_min_negative());
            let transfer_mode =
                bindings::PorterDuffTransferMode::new(env, bindings::PorterDuffMode::add(env));
            paint.set_transfer_mode(env, transfer_mode.into());
            let (mask1, mask2) = match picture.mask.choice {
                types::Choice::Setting1 => (&picture.mask.value, &picture.mask.alternative_value),
                types::Choice::Setting2 => (&picture.mask.alternative_value, &picture.mask.value),
            };
            if feather1 > 0 {
                let filter = bindings::BlurMaskFilter::new(
                    env,
                    scale * feather1 as f32,
                    bindings::Blur::normal(env),
                );
                paint.set_mask_filter(env, filter.into());
            }
            paint.set_color(env, 0xffff0000);
            let path = mask1.path_transform(env, origin, scale);
            mask_canvas.draw_path(env, path, paint);
            let filter = if feather2 > 0 {
                bindings::BlurMaskFilter::new(
                    env,
                    scale * feather2 as f32,
                    bindings::Blur::normal(env),
                )
                .into()
            } else {
                bindings::null()
            };
            paint.set_mask_filter(env, filter);
            paint.set_color(env, 0xff00ff00);
            let path = mask2.path_transform(env, origin, scale);
            mask_canvas.draw_path(env, path, paint);
            paint.set_color(env, 0xffff0000);
            paint.set_mask_filter(env, bindings::null());
            let color_matrix = env.new_float_array(20).unwrap();
            let matrix = if punch_hole {
                canvas.clip_rectangle(env, bounding_box);
                let transfer_mode = bindings::PorterDuffTransferMode::new(
                    env,
                    bindings::PorterDuffMode::destination_out(env),
                );
                paint.set_transfer_mode(env, transfer_mode.into());
                #[rustfmt::skip]
                let matrix = [
                    0., 0., 0., 0., 0.,
                    0., 0., 0., 0., 0.,
                    0., 0., 0., 0., 0.,
                    1., 1., 0., 0., -255.,
                ];
                matrix
            } else {
                paint.set_transfer_mode(env, bindings::null());
                let opacity = (picture.mask_color >> 24) as f32;
                #[rustfmt::skip]
                let matrix = [
                    0., 0., 0., 0., (picture.mask_color >> 16 & 255) as _,
                    0., 0., 0., 0., (picture.mask_color >> 8 & 255) as _,
                    0., 0., 0., 0., (picture.mask_color & 255) as _,
                    opacity / 255., opacity / 255., 0., 0., -opacity,
                ];
                matrix
            };
            env.set_float_array_region(color_matrix, 0, &matrix)
                .unwrap();
            paint.set_color_filter(
                env,
                bindings::ColorMatrixColorFilter::new(env, color_matrix.into()).into(),
            );
            canvas.draw_bitmap_simple(env, mask_image, bounding_box.point_min(), paint);
            paint.set_color_filter(env, bindings::null());
        } else {
            if feather1 > 0 {
                let filter = bindings::BlurMaskFilter::new(
                    env,
                    scale * feather1 as f32,
                    bindings::Blur::normal(env),
                );
                paint.set_mask_filter(env, filter.into());
            }
            let path = picture.mask.value.path_transform(env, origin, scale);
            if punch_hole {
                canvas.clip_rectangle(
                    env,
                    transform_bounding_box(picture.mask_bounding_box(feather1 as _)),
                );
                let transfer_mode = bindings::PorterDuffTransferMode::new(
                    env,
                    bindings::PorterDuffMode::destination_out(env),
                );
                paint.set_transfer_mode(env, transfer_mode.into());
                paint.set_color(env, 0xffffffff);
            } else {
                paint.set_color(env, picture.mask_color);
            }
            canvas.draw_path(env, path, paint);
            paint.set_mask_filter(env, bindings::null());
        }
        paint.set_transfer_mode(env, bindings::null());
    };
    let draw_color_matrix_filter = |color_matrix, feather1, feather2| {
        canvas.save(env);
        draw_feathered_mask(feather1, feather2, true);
        paint.set_color(env, 0xffffffff);
        let target_color_matrix = picture.levels.value.color_matrix(env);
        target_color_matrix.pre_concatenate(env, color_matrix);
        paint.set_color_filter(
            env,
            bindings::ColorMatrixColorFilter::with_color_matrix(env, target_color_matrix).into(),
        );
        let transfer_mode = bindings::PorterDuffTransferMode::new(
            env,
            bindings::PorterDuffMode::destination_over(env),
        );
        paint.set_transfer_mode(env, transfer_mode.into());
        canvas.draw_bitmap(env, picture.image.as_obj(), source, destination, paint);
        paint.set_color_filter(env, bindings::null());
        paint.set_transfer_mode(env, bindings::null());
        canvas.restore(env);
    };
    match picture.toolbar_visible {
        None | Some(Toolbar::Filters) | Some(Toolbar::Mask) => {
            draw_feathered_mask(0, 0, false);
        }
        Some(Toolbar::Crop) => {
            paint.set_color(env, 0xffffffff);
            let crop_size = crop.size();
            let distance_x = crop_size.x as f32 / 3.;
            let distance_y = crop_size.y as f32 / 3.;
            for index in 1..3 {
                let position_x =
                    transform_x(crop.position_x_min as f32 + distance_x * index as f32);
                canvas.draw_rectangle(
                    env,
                    position_x - 0.5,
                    transform_y(crop.position_y_min as _),
                    position_x + 0.5,
                    transform_y(crop.position_y_max as _),
                    paint,
                );
                let position_y =
                    transform_y(crop.position_y_min as f32 + distance_y * index as f32);
                canvas.draw_rectangle(
                    env,
                    transform_x(crop.position_x_min as _),
                    position_y - 0.5,
                    transform_x(crop.position_x_max as _),
                    position_y + 0.5,
                    paint,
                );
            }
        }
        Some(Toolbar::BandStop) => {
            draw_feathered_mask(
                picture.band_stop_feather1,
                picture.band_stop_feather2,
                false,
            );
        }
        Some(Toolbar::Blur) => {
            draw_feathered_mask(picture.blur_feather1, picture.blur_feather2, false);
        }
        Some(Toolbar::Clone) => {
            let mask = if picture.intersect_masks {
                if picture.mask_intersection.is_none() {
                    picture.mask_intersection = Some(types::Mask {
                        polygon: geo_booleanop::boolean::BooleanOp::intersection(
                            &picture.mask.alternative_value.polygon,
                            &picture.mask.value.polygon,
                        ),
                    });
                }
                picture.mask_intersection.as_ref().unwrap()
            } else {
                &picture.mask.value
            };
            if !mask.is_empty() {
                let path = mask.path_transform(env, origin, scale);
                if picture.clone_inpaint_outline > 0. {
                    draw_mask_bounding_box(
                        mask,
                        picture,
                        picture.clone_inpaint_context_size + picture.clone_inpaint_outline,
                    );
                    paint.set_stroke_width(env, picture.clone_inpaint_outline * scale);
                    canvas.draw_path(env, path, paint);
                    paint.set_style(env, bindings::Style::fill(env));
                }
                paint.set_color(env, 0xffffffff);
                paint.set_color_filter(env, color_filter);
                paint.set_style(env, bindings::Style::fill(env));
                canvas.save(env);
                canvas.clip_path(env, path);
                let destination = bindings::RectF::new(
                    env,
                    transform_x(picture.clone_offset.x),
                    transform_y(picture.clone_offset.y),
                    transform_x(size.x as f32 + picture.clone_offset.x),
                    transform_y(size.y as f32 + picture.clone_offset.y),
                );
                canvas.draw_bitmap(env, picture.image.as_obj(), source, destination, paint);
                paint.set_color_filter(env, bindings::null());
                canvas.restore(env);
            }
        }
        Some(Toolbar::Export)
        | Some(Toolbar::Image)
        | Some(Toolbar::Levels)
        | Some(Toolbar::Transform) => {}
        Some(Toolbar::Inpaint) => {
            let mask = if picture.intersect_masks {
                if picture.mask_intersection.is_none() {
                    picture.mask_intersection = Some(types::Mask {
                        polygon: geo_booleanop::boolean::BooleanOp::intersection(
                            &picture.mask.alternative_value.polygon,
                            &picture.mask.value.polygon,
                        ),
                    });
                }
                picture.mask_intersection.as_ref().unwrap()
            } else {
                &picture.mask.value
            };
            if !mask.is_empty() {
                let path = mask.path_transform(env, origin, scale);
                paint.set_color(env, picture.mask_color);
                canvas.draw_path(env, path, paint);
                draw_mask_bounding_box(mask, picture, picture.inpaint_context_size);
                paint.set_style(env, bindings::Style::fill(env));
            }
        }
        Some(Toolbar::LevelsFilter) => {
            draw_color_matrix_filter(
                picture.levels_filter_levels.color_matrix(env),
                picture.levels_filter_feather1,
                picture.levels_filter_feather2,
            );
        }
        Some(Toolbar::Light) => {
            if !picture.light.is_identity() {
                draw_color_matrix_filter(
                    crate::bindings::ColorMatrix::new(env, picture.light.matrix(env)),
                    picture.light_feather1,
                    picture.light_feather2,
                );
            }
        }
        Some(Toolbar::UnsharpMask) => {
            draw_feathered_mask(
                picture.unsharp_mask_feather1,
                picture.unsharp_mask_feather2,
                false,
            );
        }
    }
    match gesture {
        Some(Gesture::DrawMask(gesture)) | Some(Gesture::KeyboardDrawMask(gesture)) => {
            if gesture.points.len() > 1 {
                match &gesture.points[..] {
                    [point, remaining @ ..] => {
                        paint.set_color(env, picture.mask_color);
                        paint.set_stroke_width(env, 2. * density);
                        paint.set_style(env, bindings::Style::stroke(env));
                        let line_path = bindings::Path::new(env);
                        line_path.move_to(env, transform(point.into()));
                        for point in remaining {
                            line_path.line_to(env, transform(point.into()));
                        }
                        canvas.draw_path(env, line_path, paint);
                        paint.set_style(env, bindings::Style::fill(env));
                    }
                    _ => {
                        panic!();
                    }
                }
            }
        }
        Some(Gesture::Pan(gesture)) => {
            if gesture.split_id != split_id {
                let split = picture.split(gesture.split_id);
                let split_position_y_min = match gesture.split_id {
                    SplitId::Split1 => 0,
                    SplitId::Split2 => picture.split_position_y + 1,
                };
                let split_position_y_max = match gesture.split_id {
                    SplitId::Split1 => picture.split_position_y,
                    SplitId::Split2 => view_size_y,
                };
                paint.set_color(env, 0xffffffff);
                paint.set_stroke_width(env, 2. * density);
                paint.set_style(env, bindings::Style::stroke(env));
                canvas.draw_rectangle(
                    env,
                    transform_x(-split.pan_x / split.scale),
                    transform_y((split_position_y_min as f32 - split.pan_y) / split.scale),
                    transform_x((view_size_x as f32 - split.pan_x) / split.scale),
                    transform_y((split_position_y_max as f32 - split.pan_y) / split.scale),
                    paint,
                );
                paint.set_style(env, bindings::Style::fill(env));
            }
        }
        _ => {}
    }
}
