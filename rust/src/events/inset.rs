// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeInset(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    inset_bottom: i32,
    inset_top: i32,
) {
    let mut state = crate::state();
    let state = state.get();
    state.inset_top = inset_top;
    if inset_bottom != state.inset_bottom {
        state.inset_bottom = inset_bottom;
        state.notify_views(env);
    }
}
