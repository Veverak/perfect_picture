// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Point;
use crate::bindings;

#[derive(Clone)]
pub struct Mask {
    pub polygon: geo::MultiPolygon<f32>,
}

impl Mask {
    pub fn bounding_box(&self, context_size: f32) -> super::BoundingBox<f32> {
        let mut position_x_min = std::f32::MAX;
        let mut position_x_max = 0f32;
        let mut position_y_min = std::f32::MAX;
        let mut position_y_max = 0f32;
        if let Some(bounding_box) =
            geo::algorithm::bounding_rect::BoundingRect::bounding_rect(&self.polygon)
        {
            position_x_min = bounding_box.min().x;
            position_x_max = bounding_box.max().x;
            position_y_min = bounding_box.min().y;
            position_y_max = bounding_box.max().y;
        }
        super::BoundingBox {
            position_x_min: position_x_min - context_size,
            position_x_max: position_x_max + context_size,
            position_y_min: position_y_min - context_size,
            position_y_max: position_y_max + context_size,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.polygon.0.is_empty()
    }

    pub fn path<'a>(&self, env: jni::JNIEnv<'a>) -> bindings::Path<'a> {
        self.path_transform(env, Point { x: 0., y: 0. }, 1.)
    }

    pub fn path_transform<'a>(
        &self,
        env: jni::JNIEnv<'a>,
        origin: super::Point,
        scale: f32,
    ) -> bindings::Path<'a> {
        let transform = |point: Point| Point {
            x: origin.x + scale * point.x,
            y: origin.y + scale * point.y,
        };
        let path = bindings::Path::new(env);
        for polygon in &self.polygon.0 {
            match &polygon.exterior().0[..] {
                [point, remaining @ ..] => {
                    path.move_to(env, transform(point.into()));
                    for point in remaining {
                        path.line_to(env, transform(point.into()));
                    }
                }
                _ => {
                    panic!();
                }
            }
            for polygon in polygon.interiors() {
                match &polygon.0[..] {
                    [remaining @ .., point] => {
                        path.move_to(env, transform(point.into()));
                        for point in remaining.iter().rev() {
                            path.line_to(env, transform(point.into()));
                        }
                    }
                    _ => {
                        panic!();
                    }
                }
            }
        }
        path
    }
}
