// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::{
    action::Action,
    types::{self, Toolbar},
};

pub fn button_at_point(
    env: jni::JNIEnv,
    state: &mut crate::initialize::State,
    position_x: f32,
    position_y: f32,
    view: crate::bindings::View,
) -> Button {
    let density = view.get_display_metrics(env).scaled_density(env);
    let view_size_y = view.get_height(env);
    let toolbar_size_y = toolbar_size_y(density);
    let toolbar_count = toolbar_count(&state.picture);
    let toolbar_position_y_min = view_size_y - state.inset_bottom - toolbar_count * toolbar_size_y;
    if (position_y as i32) < toolbar_position_y_min {
        return Button::Fallthrough;
    }
    let picture = match &state.picture {
        None => {
            return Button::Some(types::ButtonGesture {
                action: Action::Open,
                bounding_box: types::BoundingBox {
                    position_x_min: 0.,
                    position_x_max: view.get_width(env) as _,
                    position_y_min: toolbar_position_y_min as _,
                    position_y_max: view_size_y as _,
                },
            })
        }
        Some(value) => value,
    };
    let toolbar_index = (position_y as i32 - toolbar_position_y_min) / toolbar_size_y;
    let mut context = super::layout::Context {
        density,
        env,
        picture,
        position_x,
        position_y_min: (toolbar_position_y_min + toolbar_index * toolbar_size_y) as _,
        position_y_max: (toolbar_position_y_min + (toolbar_index + 1) * toolbar_size_y) as _,
        toolbar_index,
        view,
    };
    fallthrough!(match picture.toolbar_visible {
        None => Button::Fallthrough,
        Some(Toolbar::BandStop) => crate::toolbars::band_stop::action_at_point(&mut context),
        Some(Toolbar::Blur) => crate::toolbars::blur::action_at_point(&mut context),
        Some(Toolbar::Clone) => crate::toolbars::clone::action_at_point(&mut context),
        Some(Toolbar::Crop) => crate::toolbars::crop::action_at_point(&mut context),
        Some(Toolbar::Export) => crate::toolbars::export::action_at_point(&mut context),
        Some(Toolbar::Filters) => crate::toolbars::filters::action_at_point(&mut context),
        Some(Toolbar::Image) => crate::toolbars::image::action_at_point(&mut context),
        Some(Toolbar::Inpaint) => crate::toolbars::inpaint::action_at_point(&mut context),
        Some(Toolbar::Levels) => crate::toolbars::levels::action_at_point(&mut context),
        Some(Toolbar::LevelsFilter) =>
            crate::toolbars::levels_filter::action_at_point(&mut context),
        Some(Toolbar::Light) => crate::toolbars::light::action_at_point(&mut context),
        Some(Toolbar::Mask) => crate::toolbars::mask::action_at_point(&mut context),
        Some(Toolbar::Transform) => crate::toolbars::transform::action_at_point(&mut context),
        Some(Toolbar::UnsharpMask) => crate::toolbars::unsharp_mask::action_at_point(&mut context),
    });
    crate::toolbars::main::action_at_point(&mut context)
}

pub fn toolbar_count(picture: &Option<types::Picture>) -> i32 {
    match picture {
        None => 1,
        Some(picture) => {
            let toolbar_size = match picture.toolbar_visible {
                None => 0,
                Some(Toolbar::BandStop) => picture.intersect_masks as i32 + 5,
                Some(Toolbar::Blur) => picture.intersect_masks as i32 + 3,
                Some(Toolbar::Clone) => 3,
                Some(Toolbar::Crop) => 4,
                Some(Toolbar::Export) => 4,
                Some(Toolbar::Filters) => 3,
                Some(Toolbar::Image) => 2,
                Some(Toolbar::Inpaint) => 2,
                Some(Toolbar::Levels) => 8,
                Some(Toolbar::LevelsFilter) => picture.intersect_masks as i32 + 6,
                Some(Toolbar::Light) => picture.intersect_masks as i32 + 6,
                Some(Toolbar::Mask) => 3,
                Some(Toolbar::Transform) => 1,
                Some(Toolbar::UnsharpMask) => picture.intersect_masks as i32 + 4,
            };
            toolbar_size + 1
        }
    }
}

pub fn toolbar_size_y(density: f32) -> i32 {
    (48. * density).round() as _
}
