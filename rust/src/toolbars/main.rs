// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::{action::Action, types::Toolbar};

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item(
            context.picture.last_filter.is_some() as usize + 3,
            |item_index| match item_index {
                0 => Action::MaskToolbar,
                1 => Action::ImageToolbar,
                2 => Action::FiltersToolbar,
                3 => Action::LastFilter,
                _ => panic!(),
            },
        );
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let items = ["Mask", "Image", "Filters"];
    let mut items_display =
        display.draw_items(items.len() + display.picture.last_filter.is_some() as usize);
    for item in items.iter() {
        items_display.draw_item(item);
    }
    if let Some(filter) = display.picture.last_filter {
        let label = match filter {
            Toolbar::BandStop => "Band-stop",
            Toolbar::Blur => "Blur",
            Toolbar::Clone => "Clone",
            Toolbar::Inpaint => "Inpaint",
            Toolbar::LevelsFilter => "Levels",
            Toolbar::Light => "Light",
            Toolbar::UnsharpMask => "Unsharp mask",
            _ => panic!(),
        };
        items_display.draw_item(label);
    }
    let selected_item_index = match display.picture.toolbar_visible {
        Some(Toolbar::Mask) => Some(0),
        Some(Toolbar::Image) => Some(1),
        Some(Toolbar::Filters) => Some(2),
        Some(_) if display.picture.toolbar_visible == display.picture.last_filter => Some(3),
        _ => None,
    };
    if let Some(item_index) = selected_item_index {
        items_display.draw_underline(item_index);
    }
}
