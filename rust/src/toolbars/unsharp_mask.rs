// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::{Button, SIZES_NONZERO, SIZES_ZERO};
use crate::{action::Action, types::Choice};

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_NONZERO,
            "Radius",
            context.picture.unsharp_mask_radius,
            |item_index| Action::UnsharpMaskRadius(SIZES_NONZERO[item_index]),
        );
    }
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_ZERO,
            "Threshold",
            context.picture.unsharp_mask_threshold,
            |item_index| Action::UnsharpMaskThreshold(SIZES_ZERO[item_index]),
        );
    }
    fallthrough!(context.feather_item(
        context.picture.unsharp_mask_feather1,
        context.picture.unsharp_mask_feather2,
        |item_index| Action::UnsharpMaskFeather(Choice::Setting1, SIZES_ZERO[item_index]),
        |item_index| Action::UnsharpMaskFeather(Choice::Setting2, SIZES_ZERO[item_index]),
    ));
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::ClearMask,
            1 => Action::ApplyUnsharpMask,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    display.draw_items_cutoff(SIZES_NONZERO, "Radius", display.picture.unsharp_mask_radius);
    display.draw_items_cutoff(
        SIZES_ZERO,
        "Threshold",
        display.picture.unsharp_mask_threshold,
    );
    display.draw_feather(
        display.picture.unsharp_mask_feather1,
        display.picture.unsharp_mask_feather2,
    );
    let mut items = display.draw_items(2);
    items.draw_item("Cancel");
    items.draw_apply_item(display.picture);
}
