// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::{
    action::Action,
    bindings::string,
    types::{Choice, Point},
};

const CROP_ASPECT_RATIOS: &[(u8, u8)] = &[(0, 0), (16, 9), (4, 3), (1, 1), (3, 4), (9, 16)];

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_label(CROP_ASPECT_RATIOS.len(), "Aspect ratio", |item_index| {
            Action::CropAspectRatio(CROP_ASPECT_RATIOS[item_index])
        });
    }
    if context.toolbar() {
        return context.opacity_item(
            "Mask opacity",
            context.picture.crop_mask_opacity,
            |item_index| {
                Action::CropMaskOpacity((255 * super::MASK_OPACITIES[item_index] as u32 / 100) as _)
            },
        );
    }
    if context.toolbar() {
        return Button::None;
    }
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::CropSetting(Choice::Setting1),
            1 => Action::CropSetting(Choice::Setting2),
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let mut items = display.draw_label(CROP_ASPECT_RATIOS.len(), "Aspect ratio");
    for aspect_ratio in CROP_ASPECT_RATIOS {
        if aspect_ratio.0 == 0 {
            items.draw_item("free");
        } else {
            items.draw_item(&format!("{}:{}", aspect_ratio.0, aspect_ratio.1));
        }
    }
    if let Some(item_index) = CROP_ASPECT_RATIOS
        .iter()
        .position(|&aspect_ratio| aspect_ratio == display.picture.crop_aspect_ratio)
    {
        items.draw_underline(item_index);
    }
    display.draw_opacity_items("Mask opacity", display.picture.crop_mask_opacity);
    display
        .paint
        .set_text_align(display.env, display.align_left);
    display
        .position_y_max
        .set(display.position_y_max.get() + display.toolbar_size_y);
    let item_position_y = display.position_y_max.get() as f32 - 20. * display.density;
    let draw_item = |label, position_x, size: crate::types::Size| {
        let text1 = string(display.env, label);
        let text2 = string(display.env, &size.format());
        let text1_size_x = display.paint.measure_text(display.env, text1);
        let text2_size_x = display.paint.measure_text(display.env, text2);
        let text2_position_x = text1_size_x + 16. * display.density;
        let position_x = position_x - (text2_position_x + text2_size_x) / 2.;
        display.canvas.draw_text(
            display.env,
            text1,
            Point {
                x: position_x,
                y: item_position_y,
            },
            display.paint,
        );
        display.canvas.draw_text(
            display.env,
            text2,
            Point {
                x: position_x + text2_position_x,
                y: item_position_y,
            },
            display.paint,
        );
    };
    draw_item(
        "Original",
        0.25 * display.view_size_x as f32,
        display.picture.size,
    );
    draw_item(
        "Crop",
        0.75 * display.view_size_x as f32,
        display.picture.crop.value.size(),
    );
    display
        .paint
        .set_text_align(display.env, display.align_center);
    display.draw_setting(display.picture.crop.choice);
}
