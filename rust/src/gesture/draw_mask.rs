// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::types;

pub struct DrawMask {
    pub points: Vec<geo::Coordinate<f32>>,
    split_id: types::SplitId,
}

impl DrawMask {
    pub fn new(point: types::Point, split: &types::Split, split_id: types::SplitId) -> Self {
        let point = geo::Coordinate {
            x: (point.x - split.pan_x) / split.scale,
            y: (point.y - split.pan_y) / split.scale,
        };
        DrawMask {
            points: vec![point],
            split_id,
        }
    }

    pub fn add_point(
        &mut self,
        env: jni::JNIEnv,
        picture: &mut types::Picture,
        position_x: f32,
        position_y: f32,
        view: crate::bindings::View,
    ) {
        let split = picture.split(self.split_id);
        let point = geo::Coordinate {
            x: (position_x - split.pan_x) / split.scale,
            y: (position_y - split.pan_y) / split.scale,
        };
        if point != *self.points.last().unwrap() {
            self.points.push(point);
            view.invalidate(env);
        }
    }

    pub fn apply(
        self,
        env: jni::JNIEnv,
        picture: &mut types::Picture,
        remove: bool,
        view: crate::bindings::View,
    ) {
        let split = picture.split(self.split_id);
        let polygon = geo::Polygon::new(self.points.into(), vec![]);
        let polygon =
            geo::algorithm::simplify::Simplify::simplify(&polygon, &(0.5 / split.scale.min(1.)));
        let mask = &mut picture.mask.value;
        mask.polygon = if remove {
            geo_booleanop::boolean::BooleanOp::difference(&mask.polygon, &polygon)
        } else {
            geo_booleanop::boolean::BooleanOp::union(&mask.polygon, &polygon)
        };
        picture.mask_intersection = None;
        view.invalidate(env);
    }
}
