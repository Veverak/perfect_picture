// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

mod back;
mod captured_pointer;
mod draw;
mod inset;
mod key_down;
mod key_up;
mod motion;
mod open;
mod scale;
mod scale_begin;
mod scale_end;
mod scroll;
mod single_tap_up;
mod touch_down;
mod touch_up;
