// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

macro_rules! fallthrough {
    ( $expression:expr ) => {
        match $expression {
            crate::toolbars::layout::Button::Fallthrough => {}
            value => return value,
        }
    };
}

mod band_stop;
mod blur;
mod clone;
mod crop;
pub mod display;
mod export;
mod filters;
mod image;
mod inpaint;
mod layout;
mod levels;
mod levels_common;
mod levels_filter;
mod light;
mod main;
mod mask;
mod toolbars;
mod transform;
mod unsharp_mask;

pub use layout::Button;
pub use toolbars::{button_at_point, toolbar_count, toolbar_size_y};

const MASK_OPACITIES: &[u16] = &[
    0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100,
];
const SIZES_ZERO: &[u16] = &[
    0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 16, 20, 24, 32, 40, 48, 64, 80, 96, 128, 160, 192, 256, 320,
    384, 512, 640, 768,
];
const SIZES_NONZERO: &[u16] = &[
    1, 2, 3, 4, 5, 6, 8, 10, 12, 16, 20, 24, 32, 40, 48, 64, 80, 96, 128, 160, 192, 256, 320, 384,
    512, 640, 768,
];
pub const TEXT_COLOR: u32 = 0xffcccccc;
pub const TEXT_SIZE: f32 = 14.;

pub fn draw(display: &mut display::Display) {
    use crate::types::Toolbar;
    match display.picture.toolbar_visible {
        None => {}
        Some(Toolbar::BandStop) => {
            band_stop::draw(display);
        }
        Some(Toolbar::Blur) => {
            blur::draw(display);
        }
        Some(Toolbar::Clone) => {
            clone::draw(display);
        }
        Some(Toolbar::Crop) => {
            crop::draw(display);
        }
        Some(Toolbar::Export) => {
            export::draw(display);
        }
        Some(Toolbar::Image) => {
            image::draw(display);
        }
        Some(Toolbar::Filters) => {
            filters::draw(display);
        }
        Some(Toolbar::Inpaint) => {
            inpaint::draw(display);
        }
        Some(Toolbar::Levels) => {
            levels::draw(display);
        }
        Some(Toolbar::LevelsFilter) => {
            levels_filter::draw(display);
        }
        Some(Toolbar::Light) => {
            light::draw(display);
        }
        Some(Toolbar::Mask) => {
            mask::draw(display);
        }
        Some(Toolbar::Transform) => {
            transform::draw(display);
        }
        Some(Toolbar::UnsharpMask) => {
            unsharp_mask::draw(display);
        }
    }
    main::draw(display);
}
