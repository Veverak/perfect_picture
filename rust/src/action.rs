// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

mod clone;
mod export;
mod filter;
mod inpaint;

use crate::{
    bindings,
    types::{BoundingBox, Choice, Format, Picture, Point, Toolbar},
};

#[derive(Clone, Copy)]
pub enum Action {
    ApplyBandStop,
    ApplyBlur,
    ApplyClone,
    ApplyInpaint,
    ApplyLevelsFilter,
    ApplyLight,
    ApplyUnsharpMask,
    BandStopCourse(u16),
    BandStopFeather(Choice, u16),
    BandStopFine(u16),
    BandStopOpacity(u8),
    BandStopToolbar,
    BlurFeather(Choice, u16),
    BlurRadius(u16),
    BlurToolbar,
    CancelClone,
    ClearMask,
    CloneInpaintContextSize(f32),
    CloneInpaintOutline(f32),
    CloneOffsetSelected,
    CloneToolbar,
    CropAspectRatio((u8, u8)),
    CropMaskOpacity(u8),
    CropSetting(Choice),
    CropToolbar,
    Export,
    ExportFormat(Format),
    ExportQuality(u8),
    ExportScaleDown(bool),
    ExportToolbar,
    FiltersToolbar,
    FlipX,
    FlipY,
    ImageToolbar,
    InpaintContextSize(f32),
    InpaintToolbar,
    IntersectMasks,
    LastFilter,
    LevelsFilterFeather(Choice, u16),
    LevelsFilterToolbar,
    LevelsSetting(Choice),
    LevelsToolbar,
    LightFeather(Choice, u16),
    LightBrightness(i8),
    LightSaturation(i8),
    LightToolbar,
    MaskColor(u32),
    MaskOpacity(u8),
    MaskSetting(Choice),
    MaskToolbar,
    Open,
    Rotate180,
    Rotate270,
    Rotate90,
    Share,
    SplitScreen,
    TransformToolbar,
    UnsharpMaskFeather(Choice, u16),
    UnsharpMaskRadius(u16),
    UnsharpMaskThreshold(u16),
    UnsharpMaskToolbar,
}

pub fn perform_action(
    action: Action,
    context: crate::bindings::Context,
    env: jni::JNIEnv,
    state: &mut crate::initialize::State,
) {
    if let Action::Open = action {
        context.open(env);
        return;
    }
    let picture = match &mut state.picture {
        None => return,
        Some(value) => value,
    };
    match action {
        Action::ApplyBandStop => {
            filter::band_stop(env, picture);
        }
        Action::ApplyBlur => {
            filter::blur(env, picture);
        }
        Action::ApplyClone => {
            clone::apply(env, picture);
        }
        Action::ApplyInpaint => {
            inpaint::apply(env, picture);
        }
        Action::ApplyLevelsFilter => {
            filter::levels_filter(env, picture);
        }
        Action::ApplyLight => {
            filter::light(env, picture);
        }
        Action::ApplyUnsharpMask => {
            filter::unsharp_mask(env, picture);
        }
        Action::BandStopCourse(value) => {
            picture.band_stop_course = value;
        }
        Action::BandStopFeather(choice, value) => {
            set_feather(
                choice,
                value,
                &mut picture.band_stop_feather1,
                &mut picture.band_stop_feather2,
                picture.intersect_masks,
            );
        }
        Action::BandStopFine(value) => {
            picture.band_stop_fine = value;
        }
        Action::BandStopOpacity(value) => {
            picture.band_stop_opacity = value;
        }
        Action::BandStopToolbar => {
            select_filter(picture, Toolbar::BandStop);
        }
        Action::BlurFeather(choice, value) => {
            set_feather(
                choice,
                value,
                &mut picture.blur_feather1,
                &mut picture.blur_feather2,
                picture.intersect_masks,
            );
        }
        Action::BlurRadius(value) => {
            picture.blur_radius = value;
        }
        Action::BlurToolbar => {
            select_filter(picture, Toolbar::Blur);
        }
        Action::CancelClone => {
            picture.clone_offset = Point { x: 0., y: 0. };
            picture.clone_offset_selected = false;
            picture.clear_mask();
        }
        Action::ClearMask => {
            picture.clear_mask();
        }
        Action::CloneInpaintContextSize(context_size) => {
            picture.clone_inpaint_context_size = context_size;
        }
        Action::CloneInpaintOutline(inpaint_outline) => {
            picture.clone_inpaint_outline = inpaint_outline;
        }
        Action::CloneOffsetSelected => {
            picture.clone_offset_selected ^= true;
        }
        Action::CloneToolbar => {
            select_filter(picture, Toolbar::Clone);
        }
        Action::CropAspectRatio(aspect_ratio) => {
            picture.crop_aspect_ratio = aspect_ratio;
        }
        Action::CropMaskOpacity(opacity) => {
            picture.crop_mask_opacity = opacity;
        }
        Action::CropSetting(choice) => {
            picture.crop.set_choice(choice);
        }
        Action::CropToolbar => {
            toggle_toolbar(picture, Toolbar::Crop);
        }
        Action::ExportFormat(format) => {
            picture.export_format = format;
        }
        Action::ExportScaleDown(scale_down) => {
            picture.export_scale_down = scale_down;
        }
        Action::Export => {
            export::export(context, env, false);
            return;
        }
        Action::ExportQuality(quality) => {
            picture.export_quality = quality;
        }
        Action::ExportToolbar => {
            toggle_toolbar(picture, Toolbar::Export);
        }
        Action::FiltersToolbar => {
            toggle_toolbar(picture, Toolbar::Filters);
        }
        Action::FlipX
        | Action::FlipY
        | Action::Rotate180
        | Action::Rotate270
        | Action::Rotate90 => {
            picture.undo = None;
            if matches!(action, Action::Rotate90 | Action::Rotate270) {
                std::mem::swap(&mut picture.size.x, &mut picture.size.y);
            }
            picture.image = context
                .transform(
                    env,
                    picture.image.as_obj(),
                    match action {
                        Action::Rotate90 => 6,
                        Action::Rotate180 => 3,
                        Action::Rotate270 => 8,
                        Action::FlipX => 2,
                        Action::FlipY => 4,
                        _ => panic!(),
                    },
                )
                .global(env);
            let size = picture.size;
            let transform = |point: &mut Point| match action {
                Action::Rotate90 => {
                    *point = Point {
                        x: size.x as f32 - point.y,
                        y: point.x,
                    };
                }
                Action::Rotate180 => {
                    point.x = size.x as f32 - point.x;
                    point.y = size.y as f32 - point.y;
                }
                Action::Rotate270 => {
                    *point = Point {
                        x: point.y,
                        y: size.y as f32 - point.y,
                    };
                }
                Action::FlipX => {
                    point.x = size.x as f32 - point.x;
                }
                Action::FlipY => {
                    point.y = size.y as f32 - point.y;
                }
                _ => panic!(),
            };
            let transform_mask = |mask: &mut crate::types::Mask| {
                let transform_polygon = |polygon: &mut geo::LineString<_>| {
                    for point in &mut polygon.0 {
                        let mut converted = Point {
                            x: point.x,
                            y: point.y,
                        };
                        transform(&mut converted);
                        point.x = converted.x;
                        point.y = converted.y;
                    }
                };
                for polygon in &mut mask.polygon {
                    polygon.exterior_mut(|polygon| transform_polygon(polygon));
                    polygon.interiors_mut(|polygons| {
                        for polygon in polygons {
                            transform_polygon(polygon);
                        }
                    });
                }
            };
            transform_mask(&mut picture.mask.alternative_value);
            transform_mask(&mut picture.mask.value);
            let crop = picture.crop.value;
            match action {
                Action::Rotate90 => {
                    picture.crop.value = BoundingBox {
                        position_x_min: picture.size.x - crop.position_y_max,
                        position_x_max: picture.size.x - crop.position_y_min,
                        position_y_min: crop.position_x_min,
                        position_y_max: crop.position_x_max,
                    };
                }
                Action::Rotate180 => {
                    picture.crop.value = BoundingBox {
                        position_x_min: picture.size.x - crop.position_x_max,
                        position_x_max: picture.size.x - crop.position_x_min,
                        position_y_min: picture.size.y - crop.position_y_max,
                        position_y_max: picture.size.y - crop.position_y_min,
                    };
                }
                Action::Rotate270 => {
                    picture.crop.value = BoundingBox {
                        position_x_min: crop.position_y_min,
                        position_x_max: crop.position_y_max,
                        position_y_min: picture.size.y - crop.position_x_max,
                        position_y_max: picture.size.y - crop.position_x_min,
                    };
                }
                Action::FlipX => {
                    picture.crop.value = BoundingBox {
                        position_x_min: picture.size.x - crop.position_x_max,
                        position_x_max: picture.size.x - crop.position_x_min,
                        ..crop
                    };
                }
                Action::FlipY => {
                    picture.crop.value = BoundingBox {
                        position_y_min: picture.size.y - crop.position_y_max,
                        position_y_max: picture.size.y - crop.position_y_min,
                        ..crop
                    };
                }
                _ => panic!(),
            }
        }
        Action::ImageToolbar => {
            toggle_toolbar(picture, Toolbar::Image);
        }
        Action::InpaintContextSize(context_size) => {
            picture.inpaint_context_size = context_size;
        }
        Action::InpaintToolbar => {
            select_filter(picture, Toolbar::Inpaint);
        }
        Action::IntersectMasks => {
            picture.intersect_masks ^= true;
        }
        Action::LastFilter => {
            if let Some(toolbar) = picture.last_filter {
                toggle_toolbar(picture, toolbar);
            }
        }
        Action::LevelsFilterFeather(choice, value) => {
            set_feather(
                choice,
                value,
                &mut picture.levels_filter_feather1,
                &mut picture.levels_filter_feather2,
                picture.intersect_masks,
            );
        }
        Action::LevelsFilterToolbar => {
            select_filter(picture, Toolbar::LevelsFilter);
        }
        Action::LevelsSetting(choice) => {
            picture.levels.set_choice(choice);
        }
        Action::LevelsToolbar => {
            toggle_toolbar(picture, Toolbar::Levels);
        }
        Action::LightFeather(choice, value) => {
            set_feather(
                choice,
                value,
                &mut picture.light_feather1,
                &mut picture.light_feather2,
                picture.intersect_masks,
            );
        }
        Action::LightBrightness(value) => {
            picture.light.brightness = value;
        }
        Action::LightSaturation(value) => {
            picture.light.saturation = value;
        }
        Action::LightToolbar => {
            select_filter(picture, Toolbar::Light);
        }
        Action::MaskColor(color) => {
            picture.mask_color = picture.mask_color & 0xff000000 | color & 0xffffff;
        }
        Action::MaskOpacity(opacity) => {
            picture.mask_color = picture.mask_color & 0xffffff | ((opacity as u32) << 24);
        }
        Action::MaskSetting(choice) => {
            picture.mask.set_choice(choice);
        }
        Action::MaskToolbar => {
            toggle_toolbar(picture, Toolbar::Mask);
        }
        Action::Open => panic!(),
        Action::Share => {
            export::export(context, env, true);
            return;
        }
        Action::SplitScreen => {
            picture.split_screen ^= true;
            if let Some(Toolbar::Image) = picture.toolbar_visible {
                picture.toolbar_visible = None;
            }
        }
        Action::TransformToolbar => {
            toggle_toolbar(picture, Toolbar::Transform);
        }
        Action::UnsharpMaskFeather(choice, value) => {
            set_feather(
                choice,
                value,
                &mut picture.unsharp_mask_feather1,
                &mut picture.unsharp_mask_feather2,
                picture.intersect_masks,
            );
        }
        Action::UnsharpMaskRadius(value) => {
            picture.unsharp_mask_radius = value;
        }
        Action::UnsharpMaskThreshold(value) => {
            picture.unsharp_mask_threshold = value;
        }
        Action::UnsharpMaskToolbar => {
            select_filter(picture, Toolbar::UnsharpMask);
        }
    }
    state.notify_views(env);
}

fn select_filter(picture: &mut Picture, toolbar: Toolbar) {
    picture.toolbar_visible = if picture.toolbar_visible == Some(toolbar) {
        None
    } else {
        picture.last_filter = Some(toolbar);
        Some(toolbar)
    };
}

fn set_feather(
    choice: Choice,
    value: u16,
    feather1: &mut u16,
    feather2: &mut u16,
    intersect_masks: bool,
) {
    if intersect_masks {
        match choice {
            Choice::Setting1 => {
                *feather1 = value;
            }
            Choice::Setting2 => {
                *feather2 = value;
            }
        }
    } else {
        *feather1 = value;
        *feather2 = value;
    }
}

fn toggle_toolbar(picture: &mut Picture, toolbar: Toolbar) {
    picture.toolbar_visible = if picture.toolbar_visible == Some(toolbar) {
        None
    } else {
        Some(toolbar)
    };
}

fn undo(env: jni::JNIEnv, picture: &mut Picture) {
    if let Some(mut undo) = picture.undo.take() {
        let size = undo.bounding_box.size();
        let image = bindings::Bitmap::create_bitmap_uninitialized(
            env,
            size,
            bindings::BitmapConfig::argb_8888(env),
        );
        let buffer = env.new_direct_byte_buffer(&mut undo.image_data).unwrap();
        image.copy_pixels_from_buffer(env, buffer.into());
        let canvas = bindings::Canvas::new(env, picture.image.as_obj());
        canvas.draw_bitmap_simple(env, image, undo.bounding_box.point_min(), bindings::null());
        picture.mask.value = undo.mask;
    }
}
