// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::bindings;
use texture_synthesis::image;

pub fn apply(env: jni::JNIEnv, picture: &mut crate::types::Picture) {
    if picture.working {
        return;
    }
    if picture.mask.value.is_empty() {
        super::undo(env, picture);
        return;
    }
    let crop;
    let intersection_mask = if picture.intersect_masks {
        let mask = picture
            .mask_intersection
            .clone()
            .unwrap_or_else(|| crate::types::Mask {
                polygon: geo_booleanop::boolean::BooleanOp::intersection(
                    &picture.mask.alternative_value.polygon,
                    &picture.mask.value.polygon,
                ),
            });
        if mask.is_empty() {
            return;
        }
        crop = crate::types::mask_bounding_box(picture.inpaint_context_size, &mask, picture);
        Some(mask)
    } else {
        crop = picture.mask_bounding_box(picture.inpaint_context_size);
        None
    };
    let mask = picture.mask.value.clone();
    let image = picture.image.clone();
    picture.working = true;
    crate::spawn(env, move |env| {
        let image = image.as_obj();
        let crop_size = crop.size();
        let cropped_image = bindings::Bitmap::create_bitmap_cropped(
            env,
            image,
            crop.position_x_min,
            crop.position_y_min,
            crop_size.x,
            crop_size.y,
        );
        let mask_image = bindings::Bitmap::create_bitmap_uninitialized(
            env,
            crop_size,
            bindings::BitmapConfig::argb_8888(env),
        );
        let canvas = bindings::Canvas::new(env, mask_image);
        canvas.draw_color(env, 0xffffffff);
        let paint = bindings::Paint::new(env);
        paint.set_color(env, 0xff000000);
        canvas.draw_path(
            env,
            intersection_mask.as_ref().unwrap_or(&mask).path_transform(
                env,
                crop.point_min_negative(),
                1.,
            ),
            paint,
        );
        let buffer_size = 4 * crop_size.x as usize * crop_size.y as usize;
        let mut image_data = vec![0; buffer_size];
        let buffer = env.new_direct_byte_buffer(&mut image_data).unwrap();
        cropped_image.copy_pixels_to_buffer(env, buffer.into());
        let undo_image_data = image_data[..].into();
        let cropped_image_buffer =
            image::RgbaImage::from_raw(crop_size.x as _, crop_size.y as _, image_data).unwrap();
        let mut image_data = vec![0; buffer_size];
        let buffer = env.new_direct_byte_buffer(&mut image_data).unwrap();
        mask_image.copy_pixels_to_buffer(env, buffer.into());
        let mask_image_buffer =
            image::RgbaImage::from_raw(crop_size.x as _, crop_size.y as _, image_data).unwrap();
        let dimensions = texture_synthesis::Dims::new(crop_size.x as _, crop_size.y as _);
        let example = texture_synthesis::Example::builder(cropped_image_buffer)
            .set_sample_method(mask_image_buffer.clone());
        let generated_image = texture_synthesis::Session::builder()
            .inpaint_example(mask_image_buffer, example, dimensions)
            .build()
            .unwrap();
        let generated_image = generated_image.run(None).into_image();
        let mut image_data = generated_image.into_raw();
        let buffer = env.new_direct_byte_buffer(&mut image_data).unwrap();
        cropped_image.copy_pixels_from_buffer(env, buffer.into());
        let canvas = bindings::Canvas::new(env, image);
        canvas.draw_bitmap_simple(env, cropped_image, crop.point_min(), bindings::null());
        let mut state_lock = crate::state();
        let state = state_lock.get();
        let picture = match &mut state.picture {
            None => return,
            Some(value) => value,
        };
        picture.clear_mask();
        picture.undo = Some(crate::types::Undo {
            bounding_box: crop,
            image_data: undo_image_data,
            mask,
        });
        picture.working = false;
        state.notify_views(env);
    });
}
