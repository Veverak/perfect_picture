// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

mod levels;
mod light;
mod mask;
mod setting;

use crate::bindings;
pub use levels::Levels;
pub use light::Light;
pub use mask::Mask;
pub use setting::{Choice, Setting};
use std::cmp::Ordering::{Equal, Greater, Less};

#[derive(Clone, Copy)]
pub struct BoundingBox<T> {
    pub position_x_min: T,
    pub position_x_max: T,
    pub position_y_min: T,
    pub position_y_max: T,
}

#[derive(Copy, Clone)]
pub struct ButtonGesture {
    pub action: crate::action::Action,
    pub bounding_box: BoundingBox<f32>,
}

#[derive(Clone, Copy)]
pub enum Format {
    Jpeg,
    Png,
    Webp,
}

pub struct Picture {
    pub band_stop_course: u16,
    pub band_stop_feather1: u16,
    pub band_stop_feather2: u16,
    pub band_stop_fine: u16,
    pub band_stop_opacity: u8,
    pub blur_feather1: u16,
    pub blur_feather2: u16,
    pub blur_radius: u16,
    pub clone_inpaint_context_size: f32,
    pub clone_inpaint_outline: f32,
    pub clone_offset: Point,
    pub clone_offset_selected: bool,
    pub crop: Setting<BoundingBox<i32>>,
    pub crop_aspect_ratio: (u8, u8),
    pub crop_mask_opacity: u8,
    pub export_format: Format,
    pub export_quality: u8,
    pub export_scale_down: bool,
    pub histogram: [[u32; 3]; 256],
    pub histogram_image: Option<bindings::GlobalBitmap>,
    pub image: bindings::GlobalBitmap,
    pub inpaint_context_size: f32,
    pub intersect_masks: bool,
    pub last_filter: Option<Toolbar>,
    pub levels: Setting<Levels>,
    pub levels_filter_feather1: u16,
    pub levels_filter_feather2: u16,
    pub levels_filter_levels: Levels,
    pub light: Light,
    pub light_feather1: u16,
    pub light_feather2: u16,
    pub mask: Setting<Mask>,
    pub mask_color: u32,
    pub mask_intersection: Option<Mask>,
    pub split1: Split,
    pub split2: Split,
    pub size: Size,
    pub split_position_y: i32,
    pub split_screen: bool,
    pub toolbar_visible: Option<Toolbar>,
    pub undo: Option<Undo>,
    pub unsharp_mask_feather1: u16,
    pub unsharp_mask_feather2: u16,
    pub unsharp_mask_radius: u16,
    pub unsharp_mask_threshold: u16,
    pub working: bool,
}

#[derive(Clone, Copy, PartialEq)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Copy)]
pub struct Size {
    pub x: i32,
    pub y: i32,
}

pub struct Split {
    pub pan_x: f32,
    pub pan_y: f32,
    pub scale: f32,
}

#[derive(Clone, Copy, PartialEq)]
pub enum SplitId {
    Split1,
    Split2,
}

#[derive(Clone, Copy, PartialEq)]
pub enum Toolbar {
    BandStop,
    Blur,
    Clone,
    Crop,
    Export,
    Filters,
    Image,
    Inpaint,
    Levels,
    LevelsFilter,
    Light,
    Mask,
    Transform,
    UnsharpMask,
}

pub struct Undo {
    pub bounding_box: BoundingBox<i32>,
    pub image_data: Box<[u8]>,
    pub mask: Mask,
}

pub fn mask_bounding_box(context_size: f32, mask: &Mask, picture: &Picture) -> BoundingBox<i32> {
    let bounding_box = mask.bounding_box(context_size);
    BoundingBox {
        position_x_min: (bounding_box.position_x_min.floor() as i32).max(0),
        position_x_max: (bounding_box.position_x_max.ceil() as i32).min(picture.size.x),
        position_y_min: (bounding_box.position_y_min.floor() as i32).max(0),
        position_y_max: (bounding_box.position_y_max.ceil() as i32).min(picture.size.y),
    }
}

impl BoundingBox<i32> {
    pub fn point_min(self) -> Point {
        Point {
            x: self.position_x_min as _,
            y: self.position_y_min as _,
        }
    }

    pub fn point_min_negative(self) -> Point {
        Point {
            x: -self.position_x_min as _,
            y: -self.position_y_min as _,
        }
    }

    pub fn size(self) -> Size {
        Size {
            x: self.position_x_max - self.position_x_min,
            y: self.position_y_max - self.position_y_min,
        }
    }
}

impl Picture {
    pub fn clear_mask(&mut self) {
        self.mask.value = Mask {
            polygon: geo::MultiPolygon(vec![]),
        };
        self.mask_intersection = None;
    }

    pub fn crop(&mut self, gesture: crate::gesture::CropEdge, position_x: f32, position_y: f32) {
        let split = self.split(gesture.split_id);
        let position_x = (position_x - split.pan_x) / split.scale;
        let position_y = (position_y - split.pan_y) / split.scale;
        let crop = &mut self.crop.value;
        if self.crop_aspect_ratio.0 == 0 {
            match gesture.attachment_x {
                Equal => {}
                Greater => {
                    crop.position_x_max = (position_x.round() as i32).min(self.size.x);
                }
                Less => {
                    crop.position_x_min = (position_x.round() as i32).max(0);
                }
            }
            match gesture.attachment_y {
                Equal => {}
                Greater => {
                    crop.position_y_max = (position_y.round() as i32).min(self.size.y);
                }
                Less => {
                    crop.position_y_min = (position_y.round() as i32).max(0);
                }
            }
        } else {
            let origin_x = match gesture.attachment_x {
                Equal => (crop.position_x_min + crop.position_x_max) as f32 / 2.,
                Greater => crop.position_x_min as f32,
                Less => crop.position_x_max as f32,
            };
            let origin_y = match gesture.attachment_y {
                Equal => (crop.position_y_min + crop.position_y_max) as f32 / 2.,
                Greater => crop.position_y_min as f32,
                Less => crop.position_y_max as f32,
            };
            let size_x_max = match gesture.attachment_x {
                Equal => {
                    let sum = crop.position_x_min + crop.position_x_max;
                    (if sum < self.size.x {
                        sum
                    } else {
                        2 * self.size.x - sum
                    }) as f32
                }
                Greater => (self.size.x - crop.position_x_min) as f32,
                Less => crop.position_x_max as f32,
            };
            let size_y_max = match gesture.attachment_y {
                Equal => {
                    let sum = crop.position_y_min + crop.position_y_max;
                    (if sum < self.size.y {
                        sum
                    } else {
                        2 * self.size.y - sum
                    }) as f32
                }
                Greater => (self.size.y - crop.position_y_min) as f32,
                Less => crop.position_y_max as f32,
            };
            let distance_x = match gesture.attachment_x {
                Equal => 2. * (position_x - origin_x).abs(),
                Greater => position_x - origin_x,
                Less => origin_x - position_x,
            };
            let distance_y = match gesture.attachment_y {
                Equal => 2. * (position_y - origin_y).abs(),
                Greater => position_y - origin_y,
                Less => origin_y - position_y,
            };
            let aspect_ratio = self.crop_aspect_ratio;
            let size = if aspect_ratio.1 as f32 * distance_x > aspect_ratio.0 as f32 * distance_y {
                let size_x = distance_x.min(size_x_max);
                let size_y = size_x * aspect_ratio.1 as f32 / aspect_ratio.0 as f32;
                if size_y > size_y_max {
                    Point {
                        x: size_y_max * aspect_ratio.0 as f32 / aspect_ratio.1 as f32,
                        y: size_y_max,
                    }
                } else {
                    Point {
                        x: size_x,
                        y: size_y,
                    }
                }
            } else {
                let size_y = distance_y.min(size_y_max);
                let size_x = size_y * aspect_ratio.0 as f32 / aspect_ratio.1 as f32;
                if size_x > size_x_max {
                    Point {
                        x: size_x_max,
                        y: size_x_max * aspect_ratio.1 as f32 / aspect_ratio.0 as f32,
                    }
                } else {
                    Point {
                        x: size_x,
                        y: size_y,
                    }
                }
            };
            let size_x = size.x.max(1.);
            let size_y = size.y.max(1.);
            match gesture.attachment_x {
                Equal => {
                    crop.position_x_min = (origin_x - size_x.round() / 2.) as i32;
                    crop.position_x_max = crop.position_x_min + size_x.round() as i32;
                }
                Greater => {
                    crop.position_x_max = (origin_x + size_x.round()) as i32;
                }
                Less => {
                    crop.position_x_min = (origin_x - size_x.round()) as i32;
                }
            }
            match gesture.attachment_y {
                Equal => {
                    crop.position_y_min = (origin_y - size_y.round() / 2.) as i32;
                    crop.position_y_max = crop.position_y_min + size_y.round() as i32;
                }
                Greater => {
                    crop.position_y_max = (origin_y + size_y.round()) as i32;
                }
                Less => {
                    crop.position_y_min = (origin_y - size_y.round()) as i32;
                }
            }
        }
    }

    pub fn mask_bounding_box(&self, context_size: f32) -> BoundingBox<i32> {
        mask_bounding_box(context_size, &self.mask.value, self)
    }

    pub fn mask_intersection_bounding_box(
        &self,
        mask1_context_size: f32,
        mask2_context_size: f32,
    ) -> Option<BoundingBox<i32>> {
        let (alternative_context_size, context_size) = match self.mask.choice {
            Choice::Setting1 => (mask2_context_size, mask1_context_size),
            Choice::Setting2 => (mask1_context_size, mask2_context_size),
        };
        let bounding_box1 = self
            .mask
            .alternative_value
            .bounding_box(alternative_context_size);
        let bounding_box2 = self.mask.value.bounding_box(context_size);
        let bounding_box = BoundingBox {
            position_x_min: (bounding_box1
                .position_x_min
                .max(bounding_box2.position_x_min)
                .floor() as i32)
                .max(0),
            position_x_max: (bounding_box1
                .position_x_max
                .min(bounding_box2.position_x_max)
                .ceil() as i32)
                .min(self.size.x),
            position_y_min: (bounding_box1
                .position_y_min
                .max(bounding_box2.position_y_min)
                .floor() as i32)
                .max(0),
            position_y_max: (bounding_box1
                .position_y_max
                .min(bounding_box2.position_y_max)
                .ceil() as i32)
                .min(self.size.y),
        };
        if bounding_box.position_x_min < bounding_box.position_x_max
            && bounding_box.position_y_min < bounding_box.position_y_max
        {
            Some(bounding_box)
        } else {
            None
        }
    }

    pub fn scaled_down_size(&self) -> Option<Size> {
        let size = self.crop.value.size();
        let target_size = if size.x > size.y {
            Size { x: 1920, y: 1080 }
        } else {
            Size { x: 1080, y: 1920 }
        };
        if size.x > target_size.x || size.y > target_size.y {
            if size.x * target_size.y > size.y * target_size.x {
                Some(Size {
                    x: target_size.x,
                    y: (target_size.x as f32 * size.y as f32 / size.x as f32).round() as _,
                })
            } else {
                Some(Size {
                    x: (target_size.y as f32 * size.x as f32 / size.y as f32).round() as _,
                    y: target_size.y,
                })
            }
        } else {
            None
        }
    }

    pub fn split(&self, split: SplitId) -> &Split {
        match split {
            SplitId::Split1 => &self.split1,
            SplitId::Split2 => &self.split2,
        }
    }

    pub fn split_at_position(&self, position_y: f32) -> Option<SplitId> {
        let position_y = position_y as i32;
        if !self.split_screen || position_y < self.split_position_y {
            Some(SplitId::Split1)
        } else if position_y > self.split_position_y {
            Some(SplitId::Split2)
        } else {
            None
        }
    }

    pub fn split_mut(&mut self, split: SplitId) -> &mut Split {
        match split {
            SplitId::Split1 => &mut self.split1,
            SplitId::Split2 => &mut self.split2,
        }
    }
}

impl From<&geo::Coordinate<f32>> for Point {
    fn from(source: &geo::Coordinate<f32>) -> Self {
        Point {
            x: source.x,
            y: source.y,
        }
    }
}

impl Size {
    pub fn format(self) -> String {
        format!("{}⨯{}", self.x, self.y)
    }
}
