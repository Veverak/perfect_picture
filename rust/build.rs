// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Write;

struct Class {
    alias: &'static str,
    name: &'static str,
    constructors: &'static [Constructor],
    fields: &'static [Field],
    methods: &'static [Method],
    static_fields: &'static [Field],
    static_methods: &'static [Method],
}

struct Constructor {
    alias: &'static str,
    argument_types: &'static [&'static str],
}

struct Field {
    alias: &'static str,
    name: &'static str,
    r#type: &'static str,
}

struct Method {
    alias: &'static str,
    name: &'static str,
    argument_types: &'static [&'static str],
    return_type: &'static str,
}

macro_rules! class {
    (
        $class_alias:ident $class_name:tt
        $( ( constructor $constructor_alias:ident $( $constructor_argument_type:tt )* ) )*
        $( ( field $field_alias:ident $field_name:ident $field_type:tt ) )*
        $( ( method $method_alias:ident $method_name:ident $method_return_type:tt $( $method_argument_type:tt )* ) )*
        $( ( static_field $static_field_alias:ident $static_field_name:ident $static_field_type:tt ) )*
        $( ( static_method $static_method_alias:ident $static_method_name:ident $static_method_return_type:tt $( $static_method_argument_type:tt )* ) )*
    ) => {
        Class {
            alias: stringify!($class_alias),
            name: $class_name,
            constructors: &[$(
                Constructor {
                    alias: stringify!($constructor_alias),
                    argument_types: &[$( $constructor_argument_type ),*],
                },
            )*],
            fields: &[$(
                Field {
                    alias: stringify!($field_alias),
                    name: stringify!($field_name),
                    r#type: $field_type,
                },
            )*],
            methods: &[$(
                Method {
                    alias: stringify!($method_alias),
                    name: stringify!($method_name),
                    argument_types: &[$( $method_argument_type ),*],
                    return_type: $method_return_type,
                },
            )*],
            static_fields: &[$(
                Field {
                    alias: stringify!($static_field_alias),
                    name: stringify!($static_field_name),
                    r#type: $static_field_type,
                },
            )*],
            static_methods: &[$(
                Method {
                    alias: stringify!($static_method_alias),
                    name: stringify!($static_method_name),
                    argument_types: &[$( $static_method_argument_type ),*],
                    return_type: $static_method_return_type,
                },
            )*],
        }
    }
}

const CLASSES: &[Class] = &[
    class!(
        Align "android/graphics/Paint$Align"
        (static_field center CENTER "Landroid/graphics/Paint$Align;")
        (static_field left LEFT "Landroid/graphics/Paint$Align;")
        (static_field right RIGHT "Landroid/graphics/Paint$Align;")
    ),
    class!(
        Bitmap "android/graphics/Bitmap"
        (method copy_pixels_from_buffer copyPixelsFromBuffer "V" "Ljava/nio/Buffer;")
        (method copy_pixels_to_buffer copyPixelsToBuffer "V" "Ljava/nio/Buffer;")
        (method get_height getHeight "I")
        (method get_width getWidth "I")
        (static_method create_bitmap_cropped createBitmap "Landroid/graphics/Bitmap;" "Landroid/graphics/Bitmap;" "I" "I" "I" "I")
        (static_method create_bitmap_initialized createBitmap "Landroid/graphics/Bitmap;" "[I" "size" "Landroid/graphics/Bitmap$Config;")
        (static_method create_bitmap_uninitialized createBitmap "Landroid/graphics/Bitmap;" "size" "Landroid/graphics/Bitmap$Config;")
    ),
    class!(
        BitmapConfig "android/graphics/Bitmap$Config"
        (static_field argb_8888 ARGB_8888 "Landroid/graphics/Bitmap$Config;")
    ),
    class!(
        Blur "android/graphics/BlurMaskFilter$Blur"
        (static_field normal NORMAL "Landroid/graphics/BlurMaskFilter$Blur;")
    ),
    class!(
        BlurMaskFilter "android/graphics/BlurMaskFilter"
        (constructor new "F" "Landroid/graphics/BlurMaskFilter$Blur;")
    ),
    class!(
        Buffer "java/nio/Buffer"
    ),
    class!(
        Bundle "android/os/Bundle"
    ),
    class!(
        CancellationSignal "android/os/CancellationSignal"
    ),
    class!(
        Canvas "android/graphics/Canvas"
        (constructor new "Landroid/graphics/Bitmap;")
        (method clip_path clipPath "Z" "Landroid/graphics/Path;")
        (method clip_rectangle clipRect "Z" "bounding_box")
        (method draw_circle drawCircle "V" "point" "F" "Landroid/graphics/Paint;")
        (method draw_color_internal drawColor "V" "I")
        (method draw_bitmap drawBitmap "V" "Landroid/graphics/Bitmap;" "Landroid/graphics/Rect;" "Landroid/graphics/RectF;" "Landroid/graphics/Paint;")
        (method draw_bitmap_simple drawBitmap "V" "Landroid/graphics/Bitmap;" "point" "Landroid/graphics/Paint;")
        (method draw_path drawPath "V" "Landroid/graphics/Path;" "Landroid/graphics/Paint;")
        (method draw_rectangle drawRect "V" "F" "F" "F" "F" "Landroid/graphics/Paint;")
        (method draw_text drawText "V" "Ljava/lang/String;" "point" "Landroid/graphics/Paint;")
        (method restore restore "V")
        (method save save "I")
        (method translate translate "V" "point")
    ),
    class!(
        ColorFilter "android/graphics/ColorFilter"
    ),
    class!(
        ColorMatrix "android/graphics/ColorMatrix"
        (constructor new "[F")
        (method pre_concatenate preConcat "V" "Landroid/graphics/ColorMatrix;")
    ),
    class!(
        ColorMatrixColorFilter "android/graphics/ColorMatrixColorFilter"
        (constructor new "[F")
        (constructor with_color_matrix "Landroid/graphics/ColorMatrix;")
    ),
    class!(
        CompressFormat "android/graphics/Bitmap$CompressFormat"
        (static_field jpeg JPEG "Landroid/graphics/Bitmap$CompressFormat;")
        (static_field png PNG "Landroid/graphics/Bitmap$CompressFormat;")
        (static_field webp WEBP "Landroid/graphics/Bitmap$CompressFormat;")
    ),
    class!(
        Context "android/content/Context"
        (method export export "V" "Landroid/graphics/Bitmap$CompressFormat;" "Landroid/graphics/Bitmap;" "Ljava/lang/String;" "I" "Z")
        (method open open "V")
        (method transform transform "Landroid/graphics/Bitmap;" "Landroid/graphics/Bitmap;" "I")
    ),
    class!(
        DisplayMetrics "android/util/DisplayMetrics"
        (field scaled_density scaledDensity "F")
    ),
    class!(
        InputStream "java/io/InputStream"
    ),
    class!(
        MaskFilter "android/graphics/MaskFilter"
    ),
    class!(
        Paint "android/graphics/Paint"
        (constructor new)
        (method measure_text measureText "F" "Ljava/lang/String;")
        (method set_color_internal setColor "V" "I")
        (method set_color_filter setColorFilter "Landroid/graphics/ColorFilter;" "Landroid/graphics/ColorFilter;")
        (method set_filter_bitmap setFilterBitmap "V" "Z")
        (method set_mask_filter setMaskFilter "Landroid/graphics/MaskFilter;" "Landroid/graphics/MaskFilter;")
        (method set_stroke_width setStrokeWidth "V" "F")
        (method set_style setStyle "V" "Landroid/graphics/Paint$Style;")
        (method set_text_align setTextAlign "V" "Landroid/graphics/Paint$Align;")
        (method set_text_size setTextSize "V" "F")
        (method set_transfer_mode setXfermode "Landroid/graphics/Xfermode;" "Landroid/graphics/Xfermode;")
    ),
    class!(
        Path "android/graphics/Path"
        (constructor new)
        (method line_to lineTo "V" "point")
        (method move_to moveTo "V" "point")
    ),
    class!(
        PorterDuffMode "android/graphics/PorterDuff$Mode"
        (static_field add ADD "Landroid/graphics/PorterDuff$Mode;")
        (static_field destination_out DST_OUT "Landroid/graphics/PorterDuff$Mode;")
        (static_field destination_over DST_OVER "Landroid/graphics/PorterDuff$Mode;")
    ),
    class!(
        PorterDuffTransferMode "android/graphics/PorterDuffXfermode"
        (constructor new "Landroid/graphics/PorterDuff$Mode;")
    ),
    class!(
        Rect "android/graphics/Rect"
        (constructor new "I" "I" "I" "I")
    ),
    class!(
        RectF "android/graphics/RectF"
        (constructor new "F" "F" "F" "F")
    ),
    class!(
        Resources "android/content/res/Resources"
        (method get_display_metrics getDisplayMetrics "Landroid/util/DisplayMetrics;")
    ),
    class!(
        String "java/lang/String"
    ),
    class!(
        Style "android/graphics/Paint$Style"
        (static_field fill FILL "Landroid/graphics/Paint$Style;")
        (static_field stroke STROKE "Landroid/graphics/Paint$Style;")
    ),
    class!(
        TransferMode "android/graphics/Xfermode"
    ),
    class!(
        Uri "android/net/Uri"
    ),
    class!(
        View "android/view/View"
        (method get_height getHeight "I")
        (method get_resources getResources "Landroid/content/res/Resources;")
        (method get_width getWidth "I")
        (method invalidate invalidate "V")
    ),
];

fn main() {
    let mut output = String::new();
    for class in CLASSES {
        output += "\n#[derive(Clone, Copy)]\n#[repr(transparent)]\npub struct ";
        output += class.alias;
        output += "<'a>(pub JObject<'a>);\n#[allow(dead_code)]\npub struct Global";
        output += class.alias;
        output += "(GlobalRef);\nimpl<'a> ";
        output += class.alias;
        output += "<'a> {\n    #[allow(dead_code)]\n    pub fn global(self, env: JNIEnv) -> Global";
        output += class.alias;
        output += " {\n        Global";
        output += class.alias;
        output += "(env.new_global_ref(self.0).unwrap())\n    }\n    #[allow(dead_code)]\n    pub fn is_null(self) -> bool {\n        self.0.is_null()\n    }\n";
        for item in class.constructors {
            output += "    pub fn ";
            output += item.alias;
            output += "(env: JNIEnv<'a>";
            for (argument_index, &argument_type) in item.argument_types.iter().enumerate() {
                write!(&mut output, ", argument{}: ", argument_index).unwrap();
                rust_type(&mut output, argument_type);
            }
            output += ") -> Self {\n        ";
            output += class.alias;
            output += "(env.new_object(\"";
            output += class.name;
            output += "\", \"(";
            for &argument_type in item.argument_types {
                output += argument_type;
            }
            output += ")V\", &[";
            argument_conversion(&mut output, item.argument_types);
            output += "]).unwrap())\n    }\n";
        }
        write_methods(&mut output, class.name, class.fields, class.methods, false);
        write_methods(
            &mut output,
            class.name,
            class.static_fields,
            class.static_methods,
            true,
        );
        output += "}\nimpl Null for ";
        output += class.alias;
        output += "<'_> {\n    fn null() -> Self {\n        ";
        output += class.alias;
        output += "(JObject::null())\n    }\n}\nimpl Global";
        output += class.alias;
        output += " {\n    #[allow(dead_code)]\n    pub fn as_obj(&self) -> ";
        output += class.alias;
        output += " {\n        ";
        output += class.alias;
        output += "(self.0.as_obj())\n    }\n}\n";
    }
    std::fs::write(
        std::path::Path::new(&std::env::var("OUT_DIR").unwrap()).join("bindings.rs"),
        &output,
    )
    .unwrap();
}

fn argument_conversion(output: &mut String, argument_types: &[&str]) {
    for (argument_index, &argument_type) in argument_types.iter().enumerate() {
        if argument_index > 0 {
            *output += ", ";
        }
        write!(&mut *output, "argument{}", argument_index).unwrap();
        match argument_type {
            "bounding_box" => {
                write!(
                    &mut *output,
                    ".position_x_min.into(), argument{}.position_y_min.into(), argument{}.position_x_max.into(), argument{}.position_y_max.into()",
                    argument_index,
                    argument_index,
                    argument_index,
                ).unwrap();
            }
            "point" => {
                write!(
                    &mut *output,
                    ".x.into(), argument{}.y.into()",
                    argument_index,
                )
                .unwrap();
            }
            "size" => {
                write!(
                    &mut *output,
                    ".x.into(), argument{}.y.into()",
                    argument_index,
                )
                .unwrap();
            }
            _ => {
                if argument_type.starts_with('L') {
                    *output += ".0";
                }
                *output += ".into()";
            }
        }
    }
}

fn class_alias(java_type: &str) -> &'static str {
    match CLASSES
        .iter()
        .find(|class| class.name == &java_type[1..java_type.len() - 1])
    {
        None => panic!("class not found: {}", java_type),
        Some(class) => class.alias,
    }
}

fn rust_type(output: &mut String, java_type: &str) {
    let rust_type = match java_type.as_bytes() {
        b"[C" | b"[F" | b"[I" | b"[J" | b"[Z" => "JObject<'a>",
        b"F" => "f32",
        b"I" => "i32",
        b"J" => "i64",
        [b'L', ..] => {
            *output += class_alias(java_type);
            "<'a>"
        }
        b"Z" => "bool",
        b"bounding_box" => "BoundingBox<i32>",
        b"point" => "Point",
        b"size" => "Size",
        _ => panic!(),
    };
    *output += rust_type;
}

fn write_methods(
    output: &mut String,
    class_name: &str,
    fields: &[Field],
    methods: &[Method],
    static_method: bool,
) {
    for item in fields {
        *output += if item.alias.ends_with("_internal") {
            "    fn "
        } else {
            "    pub fn "
        };
        *output += item.alias;
        *output += if static_method {
            "(env: JNIEnv<'a>) -> "
        } else {
            "(self, env: JNIEnv<'a>) -> "
        };
        rust_type(output, item.r#type);
        *output += " {\n        ";
        if item.r#type.starts_with('L') {
            *output += class_alias(item.r#type);
            *output += "(";
        }
        if static_method {
            *output += "env.get_static_field(\"";
            *output += class_name;
            *output += "\", \"";
        } else {
            *output += "env.get_field(self.0, \"";
        }
        *output += item.name;
        *output += "\", \"";
        *output += item.r#type;
        if item.r#type.starts_with('L') {
            *output += "\").unwrap().l().unwrap())";
        } else {
            *output += "\").unwrap().";
            *output += match item.r#type {
                "[C" | "[F" | "[I" | "[J" | "[Z" => "l",
                "F" => "f",
                "I" => "i",
                "J" => "j",
                "Z" => "z",
                _ => panic!(),
            };
            *output += "().unwrap()";
        }
        *output += "\n    }\n";
    }
    for item in methods {
        *output += if item.alias.ends_with("_internal") {
            "    fn "
        } else {
            "    pub fn "
        };
        *output += item.alias;
        *output += if static_method {
            "(env: JNIEnv<'a>"
        } else {
            "(self, env: JNIEnv<'a>"
        };
        for (argument_index, &argument_type) in item.argument_types.iter().enumerate() {
            write!(output, ", argument{}: ", argument_index).unwrap();
            rust_type(output, argument_type);
        }
        *output += ")";
        if item.return_type != "V" {
            *output += " -> ";
            rust_type(output, item.return_type);
        }
        *output += " {\n        ";
        if item.return_type.starts_with('L') {
            *output += class_alias(item.return_type);
            *output += "(";
        }
        if static_method {
            *output += "env.call_static_method(\"";
            *output += class_name;
            *output += "\", \"";
        } else {
            *output += "env.call_method(self.0, \"";
        }
        *output += item.name;
        *output += "\", \"(";
        for &argument_type in item.argument_types {
            *output += match argument_type {
                "bounding_box" => "IIII",
                "point" => "FF",
                "size" => "II",
                _ => argument_type,
            };
        }
        *output += ")";
        *output += item.return_type;
        *output += "\", &[";
        argument_conversion(output, item.argument_types);
        *output += "]).unwrap()";
        if item.return_type == "V" {
            *output += ";";
        } else if item.return_type.starts_with('L') {
            *output += ".l().unwrap())";
        } else {
            *output += ".";
            *output += match item.return_type {
                "[C" | "[F" | "[I" | "[J" | "[Z" => "l",
                "F" => "f",
                "I" => "i",
                "J" => "j",
                "Z" => "z",
                _ => panic!(),
            };
            *output += "().unwrap()";
        }
        *output += "\n    }\n";
    }
}
