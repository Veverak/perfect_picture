// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

package perfect.picture;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.GestureDetector;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowInsets;
import java.io.OutputStream;

public class Activity extends android.app.Activity {
    static Activity instance;

    static {
        System.loadLibrary("perfect_picture");
        nativeInitialize();
    }

    private native static void nativeInitialize();

    private static void notifyViews() {
        if (instance != null) {
            instance.view.postInvalidate();
        }
    }

    boolean pointerCapture;
    boolean shift;
    View view;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            openInternal(data.getData());
        }
    }

    public native void onBackPressed();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = new View(this) {
            GestureDetector gestureDetector = new GestureDetector(new GestureDetector.OnGestureListener() {
                public boolean onDown(MotionEvent event) {
                    nativeTouchDown(event.getX(), event.getY(), view);
                    return true;
                }

                public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
                    return false;
                }

                public void onLongPress(MotionEvent event) {
                }

                public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
                    int index = 0;
                    int indexMax = event2.getHistorySize();
                    while (index < indexMax) {
                        nativeScroll(event2.getHistoricalX(0, index), event2.getHistoricalY(0, index), view);
                        index++;
                    }
                    return true;
                }

                public void onShowPress(MotionEvent event) {
                }

                public boolean onSingleTapUp(MotionEvent event) {
                    nativeSingleTapUp();
                    return false;
                }
            });
            ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(Activity.this, new ScaleGestureDetector.OnScaleGestureListener() {
                public boolean onScale(ScaleGestureDetector detector) {
                    nativeScale(detector.getFocusX(), detector.getFocusY(), detector.getCurrentSpan(), view);
                    return true;
                }

                public boolean onScaleBegin(ScaleGestureDetector detector) {
                    nativeScaleBegin(detector.getFocusX(), detector.getFocusY(), detector.getCurrentSpan()); 
                    return true;
                }

                public void onScaleEnd(ScaleGestureDetector detector) {
                    nativeScaleEnd(view);
                }
            });

            {
                gestureDetector.setIsLongpressEnabled(false);
            }

            public WindowInsets onApplyWindowInsets(WindowInsets insets) {
                nativeInset(insets.getSystemWindowInsetBottom(), insets.getSystemWindowInsetTop());
                return insets;
            }

            public void onDraw(Canvas canvas) {
                nativeDraw(canvas, view);
            }

            public boolean onCapturedPointerEvent(MotionEvent event) {
                float distanceX = 0;
                float distanceY = 0;
                int index = event.getHistorySize();
                while (index > 0) {
                    index--;
                    distanceX += event.getHistoricalX(0, index);
                    distanceY += event.getHistoricalY(0, index);
                }
                nativeCapturedPointer(event.getMetaState(), distanceX, distanceY, view);
                return false;
            }

            public boolean onTouchEvent(MotionEvent event) {
                scaleGestureDetector.onTouchEvent(event);
                gestureDetector.onTouchEvent(event);
                if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                    nativeTouchUp((event.getMetaState() & 1) != 0 || shift, event.getX(), event.getY(), view);
                }
                return true;
            }
        };
        view.setDefaultFocusHighlightEnabled(false);
        view.setFocusable(true);
        setContentView(view);
        Intent intent = getIntent();
        if (Intent.ACTION_SEND.equals(intent.getAction())) {
            openInternal((Uri)intent.getParcelableExtra(Intent.EXTRA_STREAM));
        }
    }

    public boolean onGenericMotionEvent(MotionEvent event) {
        if (event.isFromSource(InputDevice.SOURCE_CLASS_POINTER)) {
            nativeMotion(event.getX(), event.getY(), view);
        }
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            shift = true;
        }
        if (event.isCtrlPressed() || event.isShiftPressed()) {
            if (!pointerCapture) {
                view.requestPointerCapture();
                pointerCapture = true;
            }
        } else if (pointerCapture) {
            view.releasePointerCapture();
            pointerCapture = false;
        }
        return super.onKeyDown(keyCode, event) || event.getRepeatCount() == 0 && nativeKeyDown(keyCode, event.getScanCode(), view) || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        super.onKeyUp(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            shift = false;
        }
        nativeKeyUp(keyCode, view);
        return false;
    }

    public void onStart() {
        super.onStart();
        instance = this;
    }

    public void onStop() {
        super.onStop();
        instance = null;
    }

    private void export(Bitmap.CompressFormat format, Bitmap image, String mimeType, int quality, boolean share) throws Exception {
        ContentResolver contentResolver = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.MIME_TYPE, mimeType);
        Uri uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        OutputStream outputStream = contentResolver.openOutputStream(uri);
        image.compress(format, quality, outputStream);
        outputStream.close();
        if (share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("image/*");
            startActivity(Intent.createChooser(intent, "Export picture"));
        }
    }

    private void open() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Open"), 0);
    }

    private void openInternal(final Uri uri) {
        new Thread() {
            public void run() {
                try {
                    ContentResolver contentResolver = getContentResolver();
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inMutable = true;
                    Bitmap image = BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, options);
                    if (image != null) {
                        int orientation = new ExifInterface(contentResolver.openInputStream(uri)).getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                        if (orientation > 1) {
                            image = transform(image, orientation);
                        }
                        nativeOpen(image);
                    }
                } catch (Exception exception) {
                    throw new RuntimeException(exception);
                }
            }
        }.start();
    }

    private Bitmap transform(Bitmap image, int orientation) {
        Matrix matrix = new Matrix();
        if (orientation == 2) {
            matrix.setScale(-1, 1);
        } else if (orientation == 3) {
            matrix.setRotate(180);
        } else if (orientation == 4) {
            matrix.setScale(1, -1);
        } else if (orientation == 6) {
            matrix.setRotate(90);
        } else if (orientation == 8) {
            matrix.setRotate(270);
        }
        return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, false);
    }

    private native void nativeCapturedPointer(int metaState, float distanceX, float distanceY, View view);

    private native void nativeDraw(Canvas canvas, View view);

    private native void nativeInset(int insetBottom, int insetTop);

    private native boolean nativeKeyDown(int keyCode, int scanCode, View view);

    private native void nativeKeyUp(int keyCode, View view);

    private native void nativeMotion(float positionX, float positionY, View view);

    private native void nativeOpen(Bitmap image);

    private native void nativeScale(float focusX, float focusY, float span, View view);

    private native void nativeScaleBegin(float focusX, float focusY, float span);

    private native void nativeScaleEnd(View view);

    private native void nativeScroll(float positionX, float positionY, View view);

    private native void nativeSingleTapUp();

    private native void nativeTouchDown(float positionX, float positionY, View view);

    private native void nativeTouchUp(boolean shift, float positionX, float positionY, View view);
}
